import { AxiosResponse } from 'axios';

export interface FightNightTracking {
  ActionId: number;
  FighterId: number | null;
  Type: string;
  RoundNumber: number | null;
  RoundTime: null | string;
  Timestamp: Date;
}
export interface OutcomeClass {
  OutcomeId: number;
  Outcome: 'Win' | 'Lose';
}
export interface WeightClassElement {
  WeightClassId: number;
  WeightClassOrder: number;
  Description: string;
  Abbreviation: string;
}
export interface Record {
  Wins: number;
  Losses: number;
  Draws: number;
  NoContests: number;
}
export interface Born {
  City: string;
  State: null | string;
  Country: string;
  TriCode: string;
}
export interface FightCardFighter {
  FighterId: number;
  MMAId: number;
  Name: string;
  Born: Born;
  FightingOutOf: Born;
  Record: Record;
  DOB: Date;
  Age: number;
  Stance: 'Orthodox' | 'Southpaw' | 'Switch';
  Weight: number;
  Height: number;
  Reach: number;
  UFCLink: string;
  WeightClasses: WeightClassElement[];
  Corner: 'Red' | 'Blue';
  WeighIn: number;
  Outcome: OutcomeClass;
  KOOfTheNight: boolean;
  SubmissionOfTheNight: boolean;
  PerformanceOfTheNight: boolean;
}

export interface Name {
  FirstName: string;
  LastName: string;
  NickName: null | string;
}

export interface Referee {
  RefereeId: number;
  FirstName: string;
  LastName: string;
}
export interface FightScoreFighter {
  FighterId: number;
  Score: number;
}
export interface FightScore {
  JudgeId: number;
  JudgeFirstName: string;
  JudgeLastName: string;
  Fighters: FightScoreFighter[];
}
export interface Result {
  Method: string;
  EndingRound: number;
  EndingTime: string;
  EndingStrike: null | string;
  EndingTarget: null | string;
  EndingPosition: null | string;
  EndingSubmission: null;
  EndingNotes: null;
  FightOfTheNight: boolean;
  FightScores: FightScore[];
}

export interface RuleSet {
  PossibleRounds: number;
  Description: '3 Rnd (5-5-5)' | '5 Rnd (5-5-5-5-5)';
}

export interface FightCardWeightClass {
  WeightClassId: number;
  CatchWeight: null;
  Weight: string;
  Description: string;
  Abbreviation: string;
}

export interface Location {
  City: string;
  State: string;
  Country: string;
  TriCode: string;
  VenueId: number;
  Venue: string;
}

export interface Organization {
  OrganizationId: number;
  Name: string;
}
export interface Accolade {
  Type: string;
  Name: string;
}
export interface FightCard {
  FightId: number;
  FightOrder: number;
  Status: 'Upcoming' | 'Live' | 'Final';
  CardSegment: 'Main' | 'Prelims1' | 'Prelims2';
  CardSegmentStartTime: Date;
  CardSegmentBroadcaster: 'ESPN' | 'PPV';
  Fighters: FightCardFighter[];
  Result: Result;
  WeightClass: FightCardWeightClass;
  Accolades: Accolade[];
  Referee: Referee;
  RuleSet: RuleSet;
  FightNightTracking: FightNightTracking[];
}
export interface LiveEventDetailInterface {
  EventId: number;
  Name: string;
  StartTime: Date;
  TimeZone: string;
  Status: 'Upcoming' | 'Live' | 'Final';
  LiveEventId: null;
  LiveFightId: null;
  LiveRoundNumber: null;
  LiveRoundElapsedTime: null;
  Organization: Organization;
  Location: Location;
  FightCard: FightCard[];
}
export default interface eventJsonInterface extends AxiosResponse {
  data: {
    LiveEventDetail: LiveEventDetailInterface;
  };
}
