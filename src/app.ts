import express from 'express';
import requestIp from 'request-ip';
import puppeteer from 'puppeteer';

// Logger
import * as logger from './utils/logger';

// Controllers
import { getNextFight } from './controllers/api/v1/nextFight';

// Utils
import { SITE_PORT } from './utils/secrets';
import { getUpcomingEvents } from './controllers/api/v1/upcomingFights';

const app = express();

const main = async () => {
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(requestIp.mw());
  app.get('/api/v1/getNextFight', getNextFight);
  app.get('/api/v1/upcomingEvents', getUpcomingEvents);

  const browser = await puppeteer.launch({ headless: false, devtools: true });
  global.browserWs = browser.wsEndpoint();
  app.listen(SITE_PORT, () => {
    logger.log(`🚀 Server ready at: https://localhost:${SITE_PORT}`);
  });
};

try {
  main();
} catch (err) {
  // eslint-disable-next-line no-console
  console.log(err);
}
