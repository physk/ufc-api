import { Request, Response } from 'express';
import scrapeNextEvents from '../../../utils/scraper/nextEvents';

// eslint-disable-next-line import/prefer-default-export
export const getUpcomingEvents = async (req: Request, res: Response) => {
  try {
    const eventList = await scrapeNextEvents();
    return res.json({ eventList });
  } catch (err) {
    console.log('Something went wrong', err);
  }
  return res.status(500).json({ status: 500, message: 'Something went wrong.' });
};
