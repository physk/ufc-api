import { Request, Response } from 'express';
import getEventJsonUrl from '../../../utils/scraper/getEventJsonUrl';
import getEventJson from '../../../utils/scraper/getEventJson';
import scrapeNextEventPage from '../../../utils/scraper/nextEventPage';

// eslint-disable-next-line import/prefer-default-export
export const getNextFight = async (req: Request, res: Response) => {
  try {
    const nextEventUrl = await scrapeNextEventPage();
    const eventJsonUrl = await getEventJsonUrl(nextEventUrl);
    const eventJson = await getEventJson(eventJsonUrl);
    return res.json({ url: nextEventUrl, eventJsonUrl, eventJson });
  } catch (err) {
    console.log('Something went wrong', err);
  }
  return res.status(500).json({ status: 500, message: 'Something went wrong.' });
};
