import { NextFunction, Request, Response } from 'express';

import debug from './debug';

export default function expressLogger(
  req: Request,
  _: Response,
  next: NextFunction
) {
  const clientIP = req.clientIp;
  debug('WEB', `[${clientIP}] ${req.method} -> ${req.path}`);
  next();
}
