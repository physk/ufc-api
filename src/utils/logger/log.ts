import kleur from 'kleur';
import moment from 'moment';

export default function log(msg: string) {
  const time = moment().format('DD-MMM-YYYY HH:mm:ss');
  // eslint-disable-next-line no-console
  console.log(`${kleur.blue(`[${time}]:`)} ${msg} `);
}
