import dotenv from 'dotenv';
import fs from 'fs-extra';

import * as logger from './logger';

const LOGGER_IDENT = 'init';
if (fs.existsSync('.env')) {
  logger.debug(
    LOGGER_IDENT,
    'Using .env file to supply config enviroment variables'
  );
  dotenv.config({ path: '.env' });
} else if (fs.existsSync('/app/.env')) {
  logger.debug(
    LOGGER_IDENT,
    'Using /app/.env file to supply config eniroment variables'
  );
  dotenv.config({ path: '/app/.env' });
} else {
  logger.debug(
    LOGGER_IDENT,
    'Using .env.sample file to supply config eniroment variables'
  );
  dotenv.config({ path: '.env.sample' });
}

export const ENVIRONMENT = process.env.NODE_ENV;
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const SITE_PORT = Number(process.env.SITE_PORT) || 3001;
export const { SITE_NAME } = process.env;
export const { BASE_URL } = process.env;

if (!BASE_URL) {
  logger.debug(LOGGER_IDENT, 'No base URL. Set BASE_URL enviroment variable');
  process.exit(1);
}
