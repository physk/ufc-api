import axios from 'axios';
import NodeCache from 'node-cache';
import * as logger from '../logger';

import { LiveEventDetailInterface } from '../../types/eventJsonInterface';

const cache = new NodeCache({ stdTTL: 60 });

export default async function getEventJson(
  eventJson: string
): Promise<LiveEventDetailInterface> {
  if (!cache.has(`json_${eventJson}`)) {
    logger.log(`Cache Miss for 'json_${eventJson}'`);
    const res = await axios.get(eventJson);
    const LiveEventDetail: LiveEventDetailInterface = res.data;
    cache.set(`json_${eventJson}`, LiveEventDetail);
  }
  logger.log(`Cache Hit for 'json_${eventJson}'`);
  const LiveEventDetail: LiveEventDetailInterface = cache.get(`json_${eventJson}`);
  return LiveEventDetail;
}
