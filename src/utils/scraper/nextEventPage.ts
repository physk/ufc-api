import scrapeNextEvents from './nextEvents';

export default async function scrapeNextEventPage(): Promise<string> {
  const eventLinks = await scrapeNextEvents();
  return eventLinks[0];
}
