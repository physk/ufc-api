import puppeteer from 'puppeteer';
import NodeCache from 'node-cache';
import * as logger from '../logger';

import { BASE_URL } from '../secrets';

const cache = new NodeCache({ stdTTL: 900 });

export default async function scrapeNextEvents(): Promise<string[]> {
  if (!cache.has('nextEvents')) {
    logger.log(`Cache Miss for 'nextEvents'`);
    const browser = await puppeteer.connect({ browserWSEndpoint: global.browserWs });
    const page = await browser.newPage();
    await page.goto(`${BASE_URL}/events`);

    const eventStubs = await page.evaluate(async () => {
      // eslint-disable-next-line no-undef
      const upcomingEvents = document.getElementById('events-list-upcoming');
      const events = upcomingEvents.querySelectorAll('[class*=result__headline]');
      const data: string[] = [];
      events.forEach((event) => {
        data.push(event.firstElementChild.getAttribute('href'));
      });

      return data;
    });
    page.close();
    const eventLinks = [];
    eventStubs.forEach((eventStub) => {
      eventLinks.push(`${BASE_URL}${eventStub}`);
    });
    cache.set('nextEvents', eventLinks);
    return eventLinks;
  }
  logger.log(`Cache Hit for 'nextEvents'`);
  const eventLinks: string[] = cache.get('nextEvents');
  return eventLinks;
}
