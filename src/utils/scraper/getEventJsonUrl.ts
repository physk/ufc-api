import NodeCache from 'node-cache';
import puppeteer from 'puppeteer';
import * as logger from '../logger';

const cache = new NodeCache({ stdTTL: 900 });

export default async function getEventJsonUrl(eventPage: string): Promise<string> {
  if (!cache.has(`event_${eventPage}`)) {
    logger.log(`Cache Miss for 'event_${eventPage}'`);
    const browser = await puppeteer.connect({ browserWSEndpoint: global.browserWs });
    const page = await browser.newPage();
    await page.goto(eventPage);

    const eventId: string = await page.evaluate(async () => {
      // eslint-disable-next-line no-undef
      const events = document.querySelectorAll('div.c-listing-ticker--footer');
      const data = [];
      events.forEach((event) => {
        data.push(event.getAttribute('data-fmid'));
      });

      return data[0];
    });
    page.close();
    cache.set(`event_${eventPage}`, eventId, 43200);
  }
  logger.log(`Cache Hit for 'event_${eventPage}'`);
  const eventId: string = cache.get(`event_${eventPage}`);
  return `https://d29dxerjsp82wz.cloudfront.net/api/v3/event/live/${eventId}.json`;
  // return `https://d29dxerjsp82wz.cloudfront.net/api/v3/event/live/975.json`;
}
